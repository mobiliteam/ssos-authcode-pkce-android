package com.celusion.pkcedemo.util;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager {

    private static SharedPreferences pref = null;
    private static SharedPreferences.Editor editor;

    private Context context;
    private static final String PREF_NAME = "PKCE";
    private static final String IS_LOGIN = "Is_Login";

    public PreferenceManager(Context context) {
        this.context = context;
        this.pref = context.getSharedPreferences(PREF_NAME, context.MODE_PRIVATE);
        this.editor = context.getSharedPreferences(PREF_NAME, context.MODE_PRIVATE).edit();
    }

    public static void setBooleanPref(String name, boolean value){
        editor.putBoolean(name,value);
        editor.apply();
    }

    public static boolean getBooleanPref(String name){
        return pref.getBoolean(name,true);
    }

    public static void setStringPref(String name, String value){
        editor.putString(name,value);
        editor.apply();
    }

    public static String getStringPref(String name){
        return pref.getString(name,"");
    }

    public static void setIntPref(String name, int value){
        editor.putInt(name,value);
        editor.apply();
    }

    public static int getIntPref(String name){
        return pref.getInt(name,0);
    }


    public static void setIsLogin(boolean isFirstTime) {
        editor.putBoolean(IS_LOGIN, isFirstTime);
        editor.apply();
    }

    public static boolean isLogin() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    public static void clearPreference(){
        editor.clear();
        editor.apply();
    }

}
