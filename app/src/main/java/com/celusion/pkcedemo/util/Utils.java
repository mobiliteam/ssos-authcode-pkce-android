package com.celusion.pkcedemo.util;

import android.util.Base64;

import java.security.MessageDigest;
import java.security.SecureRandom;

public class Utils {

    public static String randomDataBase64url() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] salt = new byte[32];
        secureRandom.nextBytes(salt);
        return base64urlencodeNoPadding(salt);
    }

    public static String base64urlencodeNoPadding(byte[] salt) {
        String base64 = Base64.encodeToString(salt, Base64.URL_SAFE | Base64.NO_WRAP | Base64.NO_PADDING);
        return base64;
    }

    public static byte[] sha256(String input) {
        try {
            byte[] inputBytes = input.getBytes("US-ASCII");
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(inputBytes, 0, inputBytes.length);
            byte[] hash = md.digest();
            return hash;
        } catch (Exception e){
            return null;
        }
    }

}
