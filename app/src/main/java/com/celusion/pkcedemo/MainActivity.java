package com.celusion.pkcedemo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import com.celusion.pkcedemo.databinding.ActivityMainBinding;
import com.celusion.pkcedemo.util.PreferenceManager;
import com.celusion.pkcedemo.util.Utils;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;
    private String codeVerifier;
    private String state;
    private String redirectUrl;
    private PreferenceManager preferenceManager;

    private OkHttpClient httpClient = new OkHttpClient.Builder().build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        preferenceManager = new PreferenceManager(MainActivity.this);
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(broadcastReceiver, new IntentFilter(WebViewActivity.RECEIVER_KEY));
        setData();
        setEvent();
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            final String resultState = bundle.getString("state");
            final String code = bundle.getString("code");
            if(resultState.equals(state)) {
                getAccessToken(code);
            }
        }
    };

    private void setData() {
        if(preferenceManager.isLogin()){
            setProfile();
        }else{
            binding.btnLogin.setText(getResources().getString(R.string.login));
            binding.llUserDetail.setVisibility(View.GONE);
        }
    }

    private void setEvent() {
        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(preferenceManager.isLogin()){
                    // do process for Logout
                }else{
                    processLogin();
                }
            }
        });
    }

    private void processLogin(){
        state = Utils.randomDataBase64url();
        codeVerifier = Utils.randomDataBase64url();
        String codeChallenge = Utils.base64urlencodeNoPadding(Utils.sha256(codeVerifier));
        redirectUrl = "http://127.0.0.1:55555/";

        String url = "http://ask.mobiliteam.in/id/connect/authorize?\n" +
                "response_type=code&\n" +
                "scope=openid profile&\n" +
                "redirect_uri="+redirectUrl+"&\n" +
                "client_id=Grasim.Ask.Desktop&\n" +
                "state="+state+"&\n" +
                "code_challenge="+codeChallenge+"&\n" +
                "code_challenge_method=S256";

        openSession(url, redirectUrl);
    }

    private void openSession(final String url, final String redirectUrl) {

        Intent intent = new Intent(this, WebViewActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString(WebViewActivity.BUNDLE_EXTRA_URL_KEY, url);
        bundle.putString(WebViewActivity.BUNDLE_EXTRA_REDIRECT_URL_KEY, redirectUrl);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(broadcastReceiver);
        super.onDestroy();
    }

    private void getAccessToken(final String code) {

        final String url = "http://ask.mobiliteam.in/id/connect/token";

        LinkedHashMap<String, String>  headers = new LinkedHashMap<>();
        headers.put("Content-Type", "application/x-www-form-urlencoded");

        LinkedHashMap<String, String> params = new LinkedHashMap<>();
        params.put("code_verifier", codeVerifier);
        params.put("code", code);
        params.put("redirect_uri", redirectUrl);
        params.put("client_id", "Grasim.Ask.Desktop");
        params.put("client_secret", "secret");
        params.put("scope", "openid profile");
        params.put("grant_type", "authorization_code");

        FormBody.Builder builder = new FormBody.Builder();
        Set<String> keys = params.keySet();
        for (String key : keys) {
            builder.add(key, params.get(key));
        }
        RequestBody requestBody = builder.build();
        Request.Builder requestBuilder = new Request.Builder()
                .url(url)
                .post(requestBody);

        if (headers != null && headers.keySet().size() > 0) {
            Headers.Builder headerBuilder = new Headers.Builder();
            Set<String> headerKeys = headers.keySet();
            for (String headerKey : headerKeys) {
                headerBuilder.add(headerKey, headers.get(headerKey));
            }
            requestBuilder.headers(headerBuilder.build());
        }

        httpClient.newCall(requestBuilder.build()).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                final int code = response.code();
                final String body = response.body().string();
                try {
                    JSONObject jsonObject = new JSONObject(body);
                    final String accessToken = jsonObject.getString("access_token");
                    final String tokenType = jsonObject.getString("token_type");
                    getUserProfile(tokenType, accessToken);
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void getUserProfile(final String tokenType, final String accessToken) {

        final String url = "http://ask.mobiliteam.in/id/connect/userinfo";

        LinkedHashMap<String, String>  headers = new LinkedHashMap<>();
        headers.put("Content-Type", "application/json");
        headers.put("Authorization", tokenType + " "+ accessToken);

        Request.Builder requestBuilder = new Request.Builder()
                .url(url).get();

        if (headers != null && headers.keySet().size() > 0) {
            Headers.Builder headerBuilder = new Headers.Builder();
            Set<String> headerKeys = headers.keySet();
            for (String headerKey : headerKeys) {
                headerBuilder.add(headerKey, headers.get(headerKey));
            }
            requestBuilder.headers(headerBuilder.build());
        }

        httpClient.newCall(requestBuilder.build()).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                final int code = response.code();
                final String body = response.body().string();
                try {
                    preferenceManager.setIsLogin(true);
                    JSONObject jsonObject = new JSONObject(body);
                    setUserProfileData(jsonObject);
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }
    private void setUserProfileData(JSONObject jsonObject){
        final String username;
        try {
            username = jsonObject.getString("preferred_username");
            final String name = jsonObject.getString("name");
            final String email = jsonObject.getString("email");

            preferenceManager.setStringPref(Constants.USER_NMAE,username);
            preferenceManager.setStringPref(Constants.NAME,name);
            preferenceManager.setStringPref(Constants.EMAIL,email);

            setProfile();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setProfile(){
        Thread thread=new Thread(){
            @Override
            public void run() {
                float i;
                try {
                    for ( i = 0; i <= 100; i++) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                    binding.llUserDetail.setVisibility(View.VISIBLE);

                                    binding.btnLogin.setText(getResources().getString(R.string.logout));
                                    binding.textUsername.setText(getResources().getString(R.string.username)+" : "+preferenceManager.getStringPref(Constants.USER_NMAE));
                                    binding.textName.setText(getResources().getString(R.string.name)+" : "+preferenceManager.getStringPref(Constants.NAME));
                                    binding.textEmail.setText(getResources().getString(R.string.email)+" : "+preferenceManager.getStringPref(Constants.EMAIL));
                            }

                        });
                        sleep(500);
                    }
                }
                catch (InterruptedException e)
                {e.printStackTrace();}
            }
        };thread.start();
    }

}
