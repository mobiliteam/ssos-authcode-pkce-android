package com.celusion.pkcedemo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.CookieManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.Set;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class WebViewActivity extends AppCompatActivity {

    public static final String RECEIVER_KEY = "result_receiver_key";

    public static final String BUNDLE_EXTRA_URL_KEY = "base_url_key";

    public static final String BUNDLE_EXTRA_REDIRECT_URL_KEY = "redirect_url_key";

    private WebView webView;

    private String url;

    private String redirectUrl;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        webView = findViewById(R.id.webView);
        setup();
    }

    private void setup() {
        webView.getSettings().setJavaScriptEnabled(true);

        Bundle bundle = getIntent().getExtras();
        url = bundle.getString(BUNDLE_EXTRA_URL_KEY);
        redirectUrl = bundle.getString(BUNDLE_EXTRA_REDIRECT_URL_KEY);

        this.clearContent();

        webView.getSettings().setUserAgentString("Mozilla/5.0 (Linux; Android 4.1.1; Galaxy Nexus Build/JRO03C) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.166 Mobile Safari/535.19");

        webView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                //Log.e(this.getClass().getSimpleName(), request.getUrl().toString());
                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                final String currentUrl = request.getUrl().toString();
                if (currentUrl.startsWith(redirectUrl)) {
                    Uri uri = Uri.parse(currentUrl);
                    Set<String> results = uri.getQueryParameterNames();
                    Bundle resultBundle = new Bundle();
                    for (String result : results) {
                        resultBundle.putString(result, uri.getQueryParameter(result));
                    }
                    Intent intent = new Intent(RECEIVER_KEY);
                    intent.putExtras(resultBundle);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                    finish();
                }
            }

        });

        webView.loadUrl(url);
    }

    private void clearContent() {
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.removeAllCookies(null);
    }

}
